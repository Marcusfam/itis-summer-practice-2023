import boto3
import configparser
import argparse
from pathlib import Path
import os


def create_arg_parser():
    parser = argparse.ArgumentParser(description='CloudPhoto argument parser')
    parser.add_argument('command', type=str, help='Input command for CloudPhoto script')
    parser.add_argument('-a', '--album', type=str, help='name of the album where photos will be uploaded')
    parser.add_argument('-p', '--path', type=str, help='the name of the directory from which the photos will be uploaded', default=".")
    args = parser.parse_args()
    return args


def create_ini_file(input_bucket, aws_access_key_id, aws_secret_access_key):
    config = configparser.ConfigParser()
    config['DEFAULT'] = {'region': 'ru-central1',
                         'endpoint_url': 'https://storage.yandexcloud.net'}
    config['DEFAULT']['bucket'] = input_bucket
    config['DEFAULT']['aws_access_key_id'] = aws_access_key_id
    config['DEFAULT']['aws_secret_access_key'] = aws_secret_access_key
    with open(f"{Path.home()}/.config/cloudphoto/cloudphotorc.ini", "w") as ini_file:
        config.write(ini_file)


def get_list_of_albums(bucket):
    objects = sorted(obj.key for obj in bucket.objects.all())
    dir_list = []
    if objects:
        for obj in objects:
            if "/" in obj and obj.split('/')[0] not in dir_list:
                dir_list.append(obj.split('/')[0])
        return dir_list
    else:
        raise RuntimeError('Photo albums not found')


def generate_index(dir_list):
    html_content = "<!DOCTYPE><html><head><title>Photo archive</title></head><body><h1>Photo archive</h1><ul>"
    for index, dir_ in enumerate(dir_list):
        html_content += f'<li><a href="album{index}.html">{dir_}</a></li>'
    html_content += "</ul></body>"
    return html_content


def generate_error():
    return '<!DOCTYPE><html><head><title>Photo archive</title></head><body><h1>Error</h1><p>Error accessing photo archive. Return to <a href="index.html">home page</a> of photo archive.</p></body></html>'


def generate_album_page(dir_, bucket):
    html_content = '<!DOCTYPE><html><head><link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/themes/classic/galleria.classic.min.css" /><style>.galleria{ width: 960px; height: 540px; background: #000 }</style><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/galleria.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/themes/classic/galleria.classic.min.js"></script></head><body><div class="galleria">'
    for photo in bucket.objects.all():
        if dir_ in photo.key:
            html_content += f'<img src={photo.key} data-title="{photo.key.split("/")[1]}">'
    html_content += '</div><p>Go back to <a href="index.html">main page</a> of photo archive</p><script>(function() {Galleria.run(".galleria");}());</script></body></html>'
    return html_content


def init():
    aws_access_key_id = input("Введите айди ключа: ")
    aws_secret_access_key = input("Введите секретный ключ: ")
    input_bucket = input("Введите название бакета: ")

    admin_session = boto3.session.Session(aws_access_key_id=aws_access_key_id,
                                          aws_secret_access_key=aws_secret_access_key, region_name="ru-central1")
    admin_resource = admin_session.resource(service_name='s3', endpoint_url='https://storage.yandexcloud.net')

    if input_bucket not in (bucket.name for bucket in admin_resource.buckets.all()):
        admin_resource.Bucket(input_bucket).create()

    create_ini_file(input_bucket, aws_access_key_id, aws_secret_access_key)


def main():
    args = create_arg_parser()
    command = args.command
    if command == "init":
        init()
    else:
        cloudphoto = CloudPhoto(args)
        cloudphoto.choose_function(command)


class CloudPhoto:
    def __init__(self, args):
        self.functions = {"list": self.list,
                          "upload": self.upload,
                          "delete": self.delete,
                          "mksite": self.mksite}
        self.types = ["jpeg", "jpg"]
        self.args = args

        open(f"{Path.home()}/.config/cloudphoto/cloudphotorc.ini", "r")
        config = configparser.ConfigParser()
        config.read(f"{Path.home()}/.config/cloudphoto/cloudphotorc.ini")

        if (
                config['DEFAULT']['bucket'] and
                config['DEFAULT']['aws_access_key_id'] and
                config['DEFAULT']['aws_secret_access_key'] and
                config['DEFAULT']['region'] and
                config['DEFAULT']['endpoint_url']
        ):
            self.bucket = config['DEFAULT']['bucket']
            self.aws_access_key_id = config['DEFAULT']['aws_access_key_id']
            self.aws_secret_access_key = config['DEFAULT']['aws_secret_access_key']
            self.region = config['DEFAULT']['region']
            self.endpoint_url = config['DEFAULT']['endpoint_url']
        else:
            raise AttributeError('configuration file does not have necessary fields')

    def choose_function(self, command):
        function = self.functions.get(command)
        if function:
            function()
        else:
            raise AttributeError(f'cloudphoto does not have a definition for command <{command}>')

    def create_connection(self):
        admin_session = boto3.session.Session(aws_access_key_id=self.aws_access_key_id,
                                              aws_secret_access_key=self.aws_secret_access_key, region_name=self.region)
        admin_resource = admin_session.resource(service_name='s3', endpoint_url=self.endpoint_url)
        bucket = admin_resource.Bucket(self.bucket)
        return bucket

    def list(self):
        bucket = self.create_connection()
        dir_list = get_list_of_albums(bucket)
        for dir_ in dir_list:
            print(dir_)

    def upload(self):
        if self.args.album:
            upload_bucket = self.create_connection()
            if os.path.isdir(self.args.path):
                flag = False
                for filename in os.listdir(self.args.path):
                    if os.path.isfile(os.path.join(self.args.path, filename)) and filename.split(".")[1] in self.types:
                        try:
                            uploader_object = upload_bucket.Object(f"{self.args.album}/{filename}")
                            uploader_object.upload_file(Filename=os.path.join(self.args.path, filename))
                            flag = True
                        except Exception:
                            raise RuntimeError(f'Warning: Photo not sent <{filename}>')
                if not flag:
                    raise RuntimeError(f'Warning: Photos not found in directory <{self.args.path}>')
            else:
                raise RuntimeError(f'Warning: No such directory <{self.args.path}>')
        else:
            raise AttributeError('Album name was not given')

    def delete(self):
        if self.args.album:
            bucket = self.create_connection()
            flag = False
            for obj in bucket.objects.all():
                if self.args.album in obj.key:
                    obj.delete()
                    flag = True
            if not flag:
                raise RuntimeError(f'Warning: Photo album not found <{self.args.album}>')
        else:
            raise AttributeError('Album name was not given')

    def mksite(self):
        bucket = self.create_connection()
        dir_list = get_list_of_albums(bucket)
        bucket.Acl().put(ACL='public-read')
        bucket_website = bucket.Website()
        index_document = {'Suffix': 'index.html'}
        error_document = {'Key': 'error.html'}
        bucket_website.put(WebsiteConfiguration={'ErrorDocument': error_document, 'IndexDocument': index_document})

        html_object = bucket.Object('index.html')
        html_object.put(Body=generate_index(dir_list), ContentType='text/html')

        html_object = bucket.Object('error.html')
        html_object.put(Body=generate_error(), ContentType='text/html')

        for index, dir_ in enumerate(dir_list):
            html_object = bucket.Object(f'album{index}.html')
            html_object.put(Body=generate_album_page(dir_, bucket), ContentType='text/html')

        print(f"https://{bucket.name}.website.yandexcloud.net/")


if __name__ == '__main__':
    main()
