"""Telegram Bot on Yandex Cloud Function."""
import os
import json
import requests
import datetime

FUNC_RESPONSE = {
    'statusCode': 200,
    'body': ''
}

TELEGRAM_BOT_TOKEN = os.environ.get("TELEGRAM_BOT_TOKEN")
WEATHER_API_KEY = os.environ.get("WEATHER_API_KEY")
GEOCODE_API_KEY = os.environ.get("GEOCODE_API_KEY")
YANDEX_SPEECHKIT_API_KEY = os.environ.get("YANDEX_SPEECHKIT_API_KEY")
FOLDER_ID = os.environ.get("FOLDER_ID")

TELEGRAM_API_URL = f"https://api.telegram.org/bot{TELEGRAM_BOT_TOKEN}"


def get_weather(lat, lon):
    response = requests.get(url=f'https://api.openweathermap.org/data/2.5/weather'
                                f'?lat={lat}&lon={lon}&appid={WEATHER_API_KEY}&lang=ru&units=metric').json()
    return response


def get_text_from_voice(message):
    file_id = message['voice']['file_id']
    file_obj = requests.post(TELEGRAM_API_URL + '/getFile', json={'file_id': file_id}).json()['result']
    file_resp = requests.get("https://api.telegram.org/file/bot" + TELEGRAM_BOT_TOKEN + f'/{file_obj["file_path"]}')

    response = requests.post(
        f'https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?folderId={FOLDER_ID}&lang=ru-RU',
        headers={'Authorization': f'Api-Key {YANDEX_SPEECHKIT_API_KEY}'},
        data=file_resp.content)

    address = json.loads(response.text)['result']
    return address


def get_location_from_string(message_text):
    response = requests.get(url=f'https://geocode-maps.yandex.ru/1.x/'
                                f'?apikey={GEOCODE_API_KEY}&geocode={message_text}&format=json').json()
    if response["response"]["GeoObjectCollection"]["metaDataProperty"]["GeocoderResponseMetaData"]["found"] == "0":
        return f"Я не нашел населенный пункт '{message_text}'."
    else:
        lon_lat = response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"].split(
            " ")
        return lon_lat


def generate_voice_message(return_message):
    response = requests.post(
        f'https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize?lang=ru-RU&voice=filipp&folderId={FOLDER_ID}',
        headers={'Authorization': f'Api-Key {YANDEX_SPEECHKIT_API_KEY}'},
        data={"text": return_message.encode('utf-8')})
    bio = b""
    for chunk in response.iter_content(chunk_size=None):
        bio += chunk
    return bio


def generate_weather_result(api_response):
    return f'{api_response["weather"][0]["description"]}.\n' \
           f'Температура {api_response["main"]["temp"]} ℃, ощущается как {api_response["main"]["feels_like"]} ℃.\n' \
           f'Атмосферное давление {api_response["main"]["pressure"]} мм рт. ст.\n' \
           f'Влажность {api_response["main"]["humidity"]} %.\n' \
           f'Видимость {api_response["visibility"]} метров.\n' \
           f'Ветер {api_response["wind"]["speed"]} м/с {get_wind_direction(api_response["wind"]["deg"])}.\n' \
           f'Восход солнца {datetime.datetime.fromtimestamp(api_response["sys"]["sunrise"] + 10800).time()} МСК. ' \
           f'Закат {datetime.datetime.fromtimestamp(api_response["sys"]["sunset"] + 10800).time()} МСК.'


def generate_weather_result_voice(api_response, address):
    return f'Населенный пункт {address}.\n' \
           f'{api_response["weather"][0]["description"]}.\n' \
           f'Температура {round(api_response["main"]["temp"])}.\n' \
           f'Ощущается как {round(api_response["main"]["feels_like"])}.\n' \
           f'Давление {round(api_response["main"]["pressure"])}.\n' \
           f'Влажность {round(api_response["main"]["humidity"])}.'


def get_wind_direction(degree):
    if (degree >= 337.5) and (degree < 22.5):
        return "C"
    elif (degree >= 22.5) and (degree < 67.5):
        return "СВ"
    elif (degree >= 67.5) and (degree < 112.5):
        return "В"
    elif (degree >= 112.5) and (degree < 157.5):
        return "ЮВ"
    elif (degree >= 157.5) and (degree < 202.5):
        return "Ю"
    elif (degree >= 202.5) and (degree < 247.5):
        return "ЮЗ"
    elif (degree >= 247.5) and (degree < 292.5):
        return "З"
    elif (degree >= 292.5) and (degree < 337.5):
        return "СЗ"


def send_message(text, message):
    """Отправка сообщения пользователю Telegram."""

    message_id = message['message_id']
    chat_id = message['chat']['id']
    reply_message = {'chat_id': chat_id,
                     'text': text,
                     'reply_to_message_id': message_id}

    requests.post(url=f'{TELEGRAM_API_URL}/sendMessage', json=reply_message)


def send_voice(voice, message):
    message_id = message['message_id']
    chat_id = message['chat']['id']

    requests.post(url=f'{TELEGRAM_API_URL}/sendVoice?chat_id={chat_id}&reply_to_message_id={message_id}&voice=', files={'voice': ("reply.ogg", voice)}, timeout=3)


def handler(event, context):
    """Обработчик облачной функции. Реализует Webhook для Telegram Bot."""

    if TELEGRAM_BOT_TOKEN is None:
        return FUNC_RESPONSE

    update = json.loads(event['body'])

    if 'message' not in update:
        return FUNC_RESPONSE

    message_in = update['message']

    if 'text' in message_in:
        message_text = message_in['text']
        if message_text in ['/start', '/help']:
            return_message = "Я сообщу вам о погоде в том месте, которое сообщите мне.\n" \
                             "Я могу ответить на:\n" \
                             "- Текстовое сообщение с названием населенного пункта.\n" \
                             "- Голосовое сообщение с названием населенного пункта.\n" \
                             "- Сообщение с точкой на карте."
            send_message(return_message, message_in)
        elif message_text[0] == "/":
            pass
        else:
            return_message = get_location_from_string(message_text)
            if isinstance(return_message, str):
                send_message(return_message, message_in)
            else:
                send_message(generate_weather_result(get_weather(return_message[1], return_message[0])), message_in)

    elif 'voice' in message_in:
        if message_in['voice']['duration'] > 30:
            return_message = "Я не могу понять голосовое сообщение длительность более 30 секунд."
            send_message(return_message, message_in)
        else:
            address = get_text_from_voice(message_in)
            if not address:
                return_message = "Не удалось распознать текст."
                send_message(return_message, message_in)
            else:
                return_message = get_location_from_string(address)
                if isinstance(return_message, str):
                    send_message(return_message, message_in)
                else:
                    send_voice(generate_voice_message(generate_weather_result_voice(get_weather(return_message[1], return_message[0]), address)), message_in)

    elif 'location' in message_in:
        lon = message_in['location']['longitude']
        lat = message_in['location']['latitude']
        api_response = get_weather(lat, lon)
        return_message = generate_weather_result(api_response)
        send_message(return_message, message_in)

    else:
        send_message('Я не могу ответить на такой тип сообщения.\n'
                     'Но могу ответить на:\n'
                     '- Текстовое сообщение с названием населенного пункта.\n'
                     '- Голосовое сообщение с названием населенного пункта.\n'
                     '- Сообщение с точкой на карте.', message_in)

    return FUNC_RESPONSE
